#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#define BUFSIZE 4096
#define MAXFILES 40

#define RL_MAX_BUF 4096

struct ReadLineBuf {
    int     fd;                 /* File descriptor from which to read */
    char    buf[RL_MAX_BUF];    /* Current buffer from file */
    int     next;               /* Index of next unread character in 'buf' */
    ssize_t len;                /* Number of characters in 'buf' */
};

void                    /* Initialize a ReadLineBuf structure */
readLineBufInit(int fd, struct ReadLineBuf *rlbuf)
{
    rlbuf->fd = fd;
    rlbuf->len = 0;
    rlbuf->next = 0;
}

/*
 * error - wrapper for perror used for bad syscalls
 */
void error(char *msg) {
  perror(msg);
  exit(1);
}

int randomFile(char * path) {
  DIR * dirp;
  struct dirent * entry;
  char files[MAXFILES][256];
  int file;
  int result;
  int file_count = 0;
  int dirfd;
  dirfd = open(path, O_RDONLY);
  dirp = fdopendir(dirfd);
  if (dirp == NULL) {
    error("Cannot open directory");
  }
  while ((entry = readdir(dirp)) != NULL) {
    if (entry->d_type == DT_REG) { /* If the entry is a regular file */
     file_count++;
     strncpy(files[file_count-1], entry->d_name, 256);
    }
  }
  srand(time(NULL));
  file = rand()%file_count;
  result = openat(dirfd, files[file], O_RDONLY);
  closedir(dirp);
  return result;
}


/* 
   Thanks to https://man7.org/tlpi/code/index.html

   Return a line of input from the buffer 'rlbuf', placing the characters in
   'buffer'. The 'n' argument specifies the size of 'buffer'. If the line of
   input is larger than this, then the excess characters are discarded. */

ssize_t
readLineBuf(struct ReadLineBuf *rlbuf, char *buffer, size_t n)
{
    size_t cnt;
    char c;

    if (n <= 0 || buffer == NULL) {
        errno = EINVAL;
        return -1;
    }

    cnt = 0;

    /* Fetch characters from rlbuf->buf, up to the next new line. */

    for (;;) {

        /* If there are insufficient characters in 'tlbuf', then obtain
           further input from the associated file descriptor. */

        if (rlbuf->next >= rlbuf->len) {
            rlbuf->len = read(rlbuf->fd, rlbuf->buf, RL_MAX_BUF);
            if (rlbuf->len == -1)
                return -1;

            if (rlbuf->len == 0)        /* End of file */
                break;

            rlbuf->next = 0;
        }

        c = rlbuf->buf[rlbuf->next];
        rlbuf->next++;

        if (cnt < n)
            buffer[cnt++] = c;

        if (c == '\n')
            break;
    }

    return cnt;
}

void process(int fd, struct sockaddr_in *clientaddr){
  char buf[BUFSIZE];
  const char * randomadrequest = "GET /randomad ";
  const char * hellorequest = "GET / ";
  const char * helloresponse = "HTTP/1.0 200 OK\r\nContent-Type: text/html\r\nConnection: close\r\n\r\nAdvertisement Server\r\n";
  const char * randomadresponse = "HTTP/1.0 200 OK\r\nContent-Type: image/jpeg\r\nConnection: close\r\n\r\n";
  const char * notfound = "HTTP/1.0 404 Not Found\r\nConnection: close\r\n\r\n";
  int filefd;
  ssize_t length;
  struct linger so_linger;

  // TODO: Add Seccomp rules here!

  /* Set the connection socket to linger */
  so_linger.l_onoff = 1;
  so_linger.l_linger = 60;
  setsockopt(fd, SOL_SOCKET, SO_LINGER,
    &so_linger, sizeof so_linger);

  /* Get the first line of the request */
  struct ReadLineBuf lineBuf;
  readLineBufInit(fd, &lineBuf);
  length = readLineBuf(&lineBuf, buf, BUFSIZE);
  if (length <= 0) {
    error("Failed to read request");
  }

  /* Is it a hello world request? */
  if (strncmp(buf, hellorequest, strlen(hellorequest)) == 0) {
    write(fd, helloresponse, strlen(helloresponse));
  } else if (strncmp(buf, randomadrequest, strlen(randomadrequest)) == 0) {
    /* A random ad request */
    printf("Random ad\n");
    write(fd, randomadresponse, strlen(randomadresponse));

    /* Get a random image */
    filefd = randomFile("images");
    if (filefd < 0) {
      error("Cannot open file");
    }
    int bytesRead = read(filefd, buf, BUFSIZE);
    while (bytesRead > 0) {
      int bytesWritten = write(fd, buf, bytesRead);
      if (bytesWritten != bytesRead) {
        /* Abort */
        printf("Failed to write data!\n");
        shutdown(fd, SHUT_WR);
        while (readLineBuf(&lineBuf, buf, BUFSIZE) > 2) { }
        close(fd);
        return;
      }
      bytesRead = read(filefd, buf, BUFSIZE);
    }
  } else {
    /* We don't know that */
    write(fd, notfound, strlen(notfound));
  }

  /* Shutdown the socket for writing */
  if (shutdown(fd, SHUT_WR) != 0) {
    printf("Failed to shutdown\n");
  }

  /* Read the remaining request headers */
  while (readLineBuf(&lineBuf, buf, BUFSIZE) > 2) { }

  /* Close the Socket */
  close(fd);
  return;
}

int main(int argc, char** argv) {
  int parentfd;                  /* parent socket */
  int childfd;                   /* child socket */
  struct sockaddr_in serveraddr; /* server's addr */
  struct sockaddr_in clientaddr; /* client addr */
  socklen_t clientlen;           /* byte size of client's address */
  int optval;                    /* flag value for setsockopt */
  pid_t pid;
  struct linger so_linger;

  if (signal(SIGCHLD, SIG_IGN) == SIG_ERR) {
    perror(0);
    exit(1);
  }

  /* open socket descriptor */
  parentfd = socket(AF_INET, SOCK_STREAM, 0);
  if (parentfd < 0)
    error("ERROR opening socket");

  /* allows us to restart server immediately */
  optval = 1;
  setsockopt(parentfd, SOL_SOCKET, SO_REUSEADDR,
       (const void *)&optval , sizeof(int));

  /* set the parent socket to linger */
  so_linger.l_onoff = 1;
  so_linger.l_linger = 60;
  setsockopt(parentfd, SOL_SOCKET, SO_LINGER,
    &so_linger, sizeof so_linger);

  /* bind port to socket */
  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short)5000);
  if (bind(parentfd, (struct sockaddr *) &serveraddr,
     sizeof(serveraddr)) < 0)
    error("ERROR on binding");

  /* get us ready to accept connection requests */
  if (listen(parentfd, 5) < 0) /* allow 5 requests to queue up */
    error("ERROR on listen");

  /*
   * main loop: wait for a connection request, parse HTTP,
   * serve requested content, close connection.
   */
  clientlen = sizeof(clientaddr);
  while (1) {
    /* wait for a connection request */
    childfd = accept(parentfd, (struct sockaddr *) &clientaddr, &clientlen);
    printf("New connection: %d\n", childfd);
    if (childfd < 0)
      error("ERROR on accept");
    pid = fork();
    if (pid < 0) {
      error("Cannot fork");
    }
    if (pid == 0) {
      // child
      close(parentfd);
      process(childfd, &clientaddr);
      return 0;
    } else {
      close(childfd);
    }
  }

}
